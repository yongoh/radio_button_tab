module RadioButtonTab
  module ApplicationHelper

    # @return [ActionView::SafeBuffer] ページコンテナ要素
    # @see RadioButtonTab::Container#initialize
    # @see RadioButtonTab::Container#create_block
    def rbtab(*args, &block)
      method_with_classes = args.delete_at(2)
      container = Container.new(self, *args)
      container.create_block(method_with_classes, &block)
      container.check!
      container.render
    end
  end
end
