window.RadioButtonTab ?= {}

class RadioButtonTab.Container

  # @!attribute [r] element
  #   @return [HTMLElement] ページコンテナ要素

  constructor: (element)->
    @element = element

  # @return [NodeList] 全てのタブ要素
  tabs: ->
    @element.querySelectorAll(":scope > input[type='radio']")

  # @return [HTMLElement] 選択されたページのタブ要素
  currentPageTab: ->
    @element.querySelector(":scope > input[type='radio']:checked")

  # @return [HTMLElement] 選択されたページの本文要素
  currentPageContent: ->
    RadioButtonTab.getContentByTab(@currentPageTab())
