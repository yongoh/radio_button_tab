window.RadioButtonTab ?= {}

RadioButtonTab.SELECTORS =
  CONTAINER: ".radio_button_tab"
  TAB: ".page-tab"
  CONTENT: ".page-content"

# @param [HTMLElement] content 本文要素
# @return [HTMLElement] `content`に対応するタブ要素
RadioButtonTab.getTabByContent = (content)->
  content.parentNode.querySelector(":scope > ##{content.id.replace(/content$/, "tab")}")

# @param [HTMLElement] tab タブ要素
# @return [HTMLElement] `tab`に対応する本文要素
RadioButtonTab.getContentByTab = (tab)->
  tab.parentNode.querySelector(":scope > ##{tab.id.replace(/tab$/, "content")}")

# キー操作でのタブの選択
document.addEventListener "keypress", (event)->
  if event.target.matches("label#{RadioButtonTab.SELECTORS.TAB}") && event.code == 'Enter'
    event.target.click()
