module RadioButtonTab
  class Page

    # @return [String] CSSのフォーマット
    CSS_FORMAT = <<~EOF
    <!--
    #%<tab>s:not(:checked) ~ #%<content>s {
      display: none;
    }
    #%<tab>s:checked ~ .page-tabs .page-tab[for='%<tab>s'] {
      border-bottom-color: transparent;
      font-weight: bold;
      cursor: inherit;
    }
    -->
    EOF
    .freeze

    # @!attribute [rw] head
    #   @return [String] タブ要素の内容
    # @!attribute [rw] options
    #   @return [Hash] オプション群
    # @!attribute [rw] html_attributes
    #   @return [Hash] 各要素のHTML属性
    # @!attribute [rw] content_proc
    #   @return [Proc] 本文要素の内容ブロック
    attr_accessor :head, :options, :html_attributes, :content_proc

    # @param [ActionView::Base] template ビューコンテキスト
    # @param [String] container_name コンテナ名
    # @param [Hash] options オプション群
    # @option options [Boolean] :checked ラジオボタンをチェック状態にするかどうか
    def initialize(template, container_name, head, options = {}, html_attributes = {}, &content_proc)
      @template, @container_name = template, container_name
      self.head, self.options = head, options
      self.html_attributes = default_html_attributes.html_attribute_merge(html_attributes)
      self.content_proc = content_proc || default_content_proc
    end

    # @return [String] スタイルシート文字列
    def css(*args)
      sprintf(CSS_FORMAT, tab: tab_id, content: content_id).html_safe
    end

    # @return [ActionView::SafeBuffer] ラジオボタン要素
    def radio_button
      checked = options.fetch(:checked, false)
      h.radio_button_tag(@container_name, object_id, checked, id: tab_id, **html_attributes)
    end

    # @return [ActionView::SafeBuffer] タブ要素
    def label
      html_attributes = {id: content_id, class: "page-tab", tabindex: 0}.html_attribute_merge(self.html_attributes)
      h.label_tag(tab_id, head, **html_attributes)
    end

    # @return [ActionView::SafeBuffer] 本文要素
    def section
      html_attributes = {id: content_id, class: "page-content"}.html_attribute_merge(self.html_attributes)
      h.content_tag(:section, html_attributes, &content_proc)
    end

    private

    def h
      @template
    end

    def tab_id
      sanitize_to_id(:tab)
    end

    def content_id
      sanitize_to_id(:content)
    end

    def sanitize_to_id(*keys)
      [@container_name, object_id, *keys].map do |key|
        h.send(:sanitize_to_id, key).gsub(/\W/, "_")
      end.join("-")
    end

    # @return [Hash] HTML属性のデフォルト
    def default_html_attributes
      {}
    end

    # @return [Proc] 内容ブロックのデフォルト
    def default_content_proc
      proc{}
    end
  end
end
