module RadioButtonTab
  class Container
    include Enumerable

    # @!attribute [r] name
    #   @return [String] コンテナ名（ラジオボタン要素の`name`属性値）
    # @!attribute [r] html_attributes
    #   @return [Hash] HTML属性
    attr_reader :name, :html_attributes
    delegate *(Array.instance_methods - Enumerable.instance_methods - Object.instance_methods), to: :@pages

    # @param [ActionView::Base] template ビューコンテキスト
    # @param [Hash] html_attributes 追加するHTML属性
    def initialize(template, name, html_attributes = {})
      @template, @name = template, name.to_s
      @html_attributes = default_html_attributes.html_attribute_merge(html_attributes)
      @pages = []
    end

    # ページを作成して追加
    # @param [Class] page_class ページクラス
    # @return [Object] `page_class`のインスタンス
    def create(page_class, *args, &block)
      build_method_name = page_class.respond_to?(:build) ? :build : :new
      page = page_class.public_send(build_method_name, @template, name, *args, &block)
      self << page
      page
    end

    # ブロック内でページを作成して追加
    # @example
    #   container.create_block(foo: Foo, bar: Bar, baz: Baz) do |builder|
    #     builder.foo("FOO")
    #     builder.bar("BAR")
    #     builder.baz("BAZ")
    #   end
    # @see RadioButtonTab::Builder::class_build
    def create_block(*args)
      yield(Builder.class_build(*args).new(self))
      self
    end

    # @return [ActionView::SafeBuffer] スタイルシート文字列群を含むスタイルシート要素
    def style
      h.content_tag(:style) do
        each do |page|
          h.concat(page.css)
        end
      end
    end

    # @return [ActionView::SafeBuffer] ラジオボタン要素群
    def radio_buttons
      h.capture do
        each do |page|
          h.concat(page.radio_button)
        end
      end
    end

    # @return [ActionView::SafeBuffer] タブ群を含む要素
    def menu
      h.content_tag(:menu, class: "page-tabs") do
        each do |page|
          h.concat(page.label)
        end
      end
    end

    # @return [ActionView::SafeBuffer] 本文要素群
    def sections
      h.capture do
        each do |page|
          h.concat(page.section)
        end
      end
    end

    # @return [ActionView::SafeBuffer] ページコンテナ要素
    def render
      h.content_tag(:div, html_attributes) do
        h.concat(style)
        h.concat(radio_buttons)
        h.concat(menu)
        h.concat(sections)
      end
    end

    # オプション`:checked`が`true`のページが無い場合、先頭のページを`true`にする
    def check!
      bool = any? && !any?{|page| page.options[:checked] }
      first.options[:checked] = true if bool
      bool
    end

    private

    def h
      @template
    end

    def default_html_attributes
      {
        class: "radio_button_tab",
        id: h.send(:sanitize_to_id, name),
      }
    end
  end
end
