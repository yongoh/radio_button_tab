module RadioButtonTab
  class Builder

    # @return [Hash] ページ作成メソッド名とページクラスのペアのハッシュのデフォルト
    DEFAULT_METHOD_WITH_CLASSES = {page: Page}.freeze

    class << self

      # @param [Hash<Symbol => Class>] method_with_classes ページ作成メソッド名とページクラスのペアのハッシュ
      # @return [Class<RadioButtonTab::Builder>] このクラスのサブクラス
      def class_build(method_with_classes = nil)
        Class.new(Builder) do
          (method_with_classes || DEFAULT_METHOD_WITH_CLASSES).each do |method_name, page_class|

            # @see RadioButtonTab::Container#create
            define_method(method_name) do |*args, &block|
              @container.create(page_class, *args, &block)
            end
          end
        end
      end
    end

    # @param [RadioButtonTab::Container] container コンテナオブジェクト
    def initialize(container)
      @container = container
    end
  end
end
