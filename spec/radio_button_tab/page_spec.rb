require 'rails_helper'

describe RadioButtonTab::Page do
  let(:page){ described_class.new(view_context, "foo[bar]", "baz") }

  describe "#css" do
    it "は、各要素のセレクタを含むスタイルシート文字列を返すこと" do
      expect(page.css).to match(/#foo_bar-\d+-tab\b/).and match(/\.page-tab\[for='foo_bar-\d+-tab'\]/).and match(/#foo_bar-\d+-content\b/)
    end
  end

  describe "#radio_button" do
    subject{ page.radio_button }

    it "は、ラジオボタン要素を返すこと" do
      is_expected.to have_tag("input[id][value]", with: {type: "radio", name: "foo[bar]"})
    end

    context "`#initialize`のオプション`:checked`に`false`を渡していた場合 (default)" do
      it "は、非チェック状態のラジオボタン要素を返すこと" do
        is_expected.to have_tag("input:not([checked])", with: {type: "radio"})
      end
    end

    context "`#initialize`のオプション`:checked`に`true`を渡していた場合" do
      let(:page){ described_class.new(view_context, "foo[bar]", "baz", checked: true) }

      it "は、チェック状態のラジオボタン要素を返すこと" do
        is_expected.to have_tag("input[checked]", with: {type: "radio"})
      end
    end
  end

  describe "#label" do
    subject{ page.label }

    it "は、タブ要素を返すこと" do
      is_expected.to have_tag("label.page-tab[for]", with: {tabindex: "0"}, text: "baz")
    end
  end

  describe "#section" do
    subject{ page.section }

    it "は、本文要素を返すこと" do
      is_expected.to have_tag("section.page-content[id]")
    end

    context "`#initialize`にブロックを渡さなかった場合 (default)" do
      it "は、空の本文要素を返すこと" do
        is_expected.to have_tag(".page-content:empty")
      end
    end

    context "`#initialize`に渡したブロック内でHTML要素を作成した場合" do
      let(:page){
        described_class.new(view_context, "foo[bar]", "baz") do
          h.concat h.content_tag(:div, "(´･ω･｀)", id: "shobon")
          h.concat h.content_tag(:span, "（＾ω＾）", class: "boon")
        end
      }

      it "は、HTML要素を内容として持つ本文要素を返すこと" do
        is_expected.to have_tag(".page-content"){
          with_tag("div#shobon", text: "(´･ω･｀)")
          with_tag("span.boon", text: "（＾ω＾）")
        }
      end
    end
  end

  context "`#initialize`の第4引数にHTML属性ハッシュを渡した場合" do
    let(:page){ described_class.new(view_context, "foo[bar]", "baz", {}, id: "my-id", class: "my-class", name: "my-name", for: "my-for") }

    describe "#radio_button" do
      it{ expect(page.radio_button).to have_tag("input.my-class#my-id[value]", with: {type: "radio", name: "my-name"}) }
    end

    describe "#label" do
      it{ expect(page.label).to have_tag(".page-tab.my-class#my-id", with: {for: "my-for"}) }
    end

    describe "#section" do
      it{ expect(page.section).to have_tag(".page-content.my-class#my-id") }
    end
  end
end
