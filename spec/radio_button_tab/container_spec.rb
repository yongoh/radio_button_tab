require 'rails_helper'

describe RadioButtonTab::Container do
  let(:container){ described_class.new(view_context, "foo[bar]") }

  describe "#style" do
    before do
      container << double("Page", css: "AAA")
      container << double("Page", css: "BBB")
      container << double("Page", css: "CCC")
    end

    it "は、各ページの`#css`の結果を含むスタイルシート要素を返すこと" do
      expect(container.style).to have_tag("style", count: 1, text: "AAABBBCCC")
    end
  end

  describe "#radio_buttons" do
    before do
      container << double("Page", radio_button: "AAA")
      container << double("Page", radio_button: "BBB")
      container << double("Page", radio_button: "CCC")
    end

    it "は、各ページの`#radio_button`の結果を連結したものを返すこと" do
      expect(container.radio_buttons).to eq("AAABBBCCC")
    end
  end

  describe "#menu" do
    before do
      container << double("Page", label: "AAA")
      container << double("Page", label: "BBB")
      container << double("Page", label: "CCC")
    end

    it "は、各ページの`#label`の結果を含む要素を返すこと" do
      expect(container.menu).to have_tag("menu.page-tabs", count: 1, text: "AAABBBCCC")
    end
  end

  describe "#sections" do
    before do
      container << double("Page", section: "AAA")
      container << double("Page", section: "BBB")
      container << double("Page", section: "CCC")
    end

    it "は、各ページの`#section`の結果を連結したものを返すこと" do
      expect(container.sections).to eq("AAABBBCCC")
    end
  end

  describe "#check!" do
    subject{ container.check! }

    context "オプション`:checked`に`true`を持つページが無い場合" do
      before do
        container << double("Page", options: {})
        container << double("Page", options: {})
        container << double("Page", options: {})
      end

      it "は、先頭のページのオプション`:checked`を`true`にすること" do
        subject
        expect(container).to match([
          have_attributes(options: match(checked: true)),
          have_attributes(options: {}),
          have_attributes(options: {}),
        ])
      end

      it{ is_expected.to equal(true) }
    end

    context "オプション`:checked`に`true`を持つページがある場合" do
      before do
        container << double("Page", options: {})
        container << double("Page", options: {checked: true})
        container << double("Page", options: {})
      end

      it "は、各ページを変化させないこと" do
        subject
        expect(container).to match([
          have_attributes(options: {}),
          have_attributes(options: match(checked: true)),
          have_attributes(options: {}),
        ])
      end

      it{ is_expected.to equal(false) }
    end

    context "コンテナが空の場合" do
      it{ is_expected.to equal(false) }
    end
  end

  describe "#create" do
    subject{ container.create(klass, "A", "B", "C") }

    let(:klass){ Struct.new(:template, :container_name, :a, :b, :c) }

    it "は、第1引数のクラスに第2引数以降を渡してインスタンス化したものをコンテナに追加すること" do
      subject
      expect(container.last).to be_instance_of(klass).and have_attributes(
        template: equal(view_context),
        container_name: "foo[bar]",
        a: "A",
        b: "B",
        c: "C",
      )
    end

    it{ is_expected.to equal(container.last) }

    context "第1引数のクラスが`::build`を持つ場合" do
      let(:klass){ double("PageClass") }

      it "は、`::build`に移譲すること" do
        expect(klass).to receive(:build).with(equal(view_context), eq("foo[bar]"), eq("baz"))
        container.create(klass, "baz")
      end
    end

    context "第1引数のクラスが`::build`を持たない場合" do
      it "は、`::new`に移譲すること" do
        expect(klass).to receive(:new).with(equal(view_context), eq("foo[bar]"), eq("baz"))
        container.create(klass, "baz")
      end
    end
  end

  describe "#create_block" do
    it{ expect{|block| container.create_block(&block) }.to yield_with_args(be_a(RadioButtonTab::Builder)) }

    it "は、ブロック引数のメソッドで作成したページをコンテナに追加すること" do
      container.create_block do |builder|
        builder.page("aaa")
        builder.page("bbb")
        builder.page("ccc")
      end

      expect(container).to match([
        be_instance_of(RadioButtonTab::Page).and(have_attributes(head: "aaa")),
        be_instance_of(RadioButtonTab::Page).and(have_attributes(head: "bbb")),
        be_instance_of(RadioButtonTab::Page).and(have_attributes(head: "ccc")),
      ])
    end

    context "引数にシンボルとクラスのペアのハッシュを渡した場合" do
      %w(foo bar baz).each do |xxx|
        let("#{xxx}_page_class"){ Struct.new(:template, :container_name, :head) }
      end

      it "は、キーと同じメソッド名で値のクラスでページを作成できるようにすること" do
        container.create_block(foo: foo_page_class, bar: bar_page_class, baz: baz_page_class) do |builder|
          builder.foo("aaa")
          builder.bar("bbb")
          builder.baz("ccc")
          builder.baz("ddd")
        end

        expect(container).to match([
          be_instance_of(foo_page_class).and(have_attributes(head: "aaa")),
          be_instance_of(bar_page_class).and(have_attributes(head: "bbb")),
          be_instance_of(baz_page_class).and(have_attributes(head: "ccc")),
          be_instance_of(baz_page_class).and(have_attributes(head: "ddd")),
        ])
      end

      it "は、デフォルトのページ作成メソッドは使えなくなること" do
        expect{
          container.create_block({}){|builder| builder.page("aaa") }
        }.to raise_error(NoMethodError, /^undefined method `page'/)
      end
    end
  end
end
