RSpec.configure do |config|
  config.include(Module.new do
    def view_context
      @view_context ||= Class.new(ActionView::Base) do
        include Rails.application.routes.url_helpers
        include RadioButtonTab::ApplicationHelper

        def default_url_options
          {host: 'www.example.com'}
        end
      end.new
    end

    alias_method :h, :view_context
  end)
end
