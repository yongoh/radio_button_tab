require 'rails_helper'

describe RadioButtonTab::ApplicationHelper do
  describe "#rbtab" do
    subject do
      h.rbtab("hoge[piyo]") do |builder|
        concat(builder.page("FOO"){ "foo内容" })
        concat(builder.page("BAR"){ "bar内容" })
        concat(builder.page("BAZ", {}, class: "my-class"){ "baz内容" })
      end
    end

    it "は、ページコンテナ要素を返すこと" do
      is_expected.to have_tag("div.radio_button_tab[id]"){
        with_tag("style", count: 1){
          with_text(/#hoge_piyo-\d+-tab\b/)
          with_text(/\.page-tab\[for='hoge_piyo-\d+-tab'\]/)
          with_text(/#hoge_piyo-\d+-content\b/)
        }

        with_tag("input[id][value]", with: {type: "radio", name: "hoge[piyo]"}, count: 3)
        with_tag("input:nth-of-type(1):not(.my-class)", with: {checked: "checked"})
        with_tag("input:nth-of-type(2):not(.my-class):not([checked])")
        with_tag("input:nth-of-type(3).my-class:not([checked])")

        with_tag(".page-tabs", count: 1){
          with_tag(".page-tab[for]", count: 3)
          with_tag(".page-tab:not(.my-class)", text: "FOO")
          with_tag(".page-tab:not(.my-class)", text: "BAR")
          with_tag(".page-tab.my-class", text: "BAZ")
        }

        with_tag(".page-content[id]", count: 3)
        with_tag(".page-content:not(.my-class)", text: "foo内容")
        with_tag(".page-content:not(.my-class)", text: "bar内容")
        with_tag(".page-content.my-class", text: "baz内容")
      }
    end
  end
end
