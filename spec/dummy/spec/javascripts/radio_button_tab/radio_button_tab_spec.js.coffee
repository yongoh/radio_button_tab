describe "RadioButtonTab", ->
  beforeEach ->
    loadFixtures("radio_button_tab.html")

  describe "::getTabByContent", ->
    it "は、渡した本文要素に対応するタブ要素を返すこと", ->
      content = document.getElementById("hoge_piyo-foo-content")
      expect(RadioButtonTab.getTabByContent(content)).toHaveId("hoge_piyo-foo-tab")

  describe "::getContentByTab", ->
    it "は、渡したタブ要素に対応する本文要素を返すこと", ->
      tab = document.getElementById("hoge_piyo-foo-tab")
      expect(RadioButtonTab.getContentByTab(tab)).toHaveId("hoge_piyo-foo-content")
