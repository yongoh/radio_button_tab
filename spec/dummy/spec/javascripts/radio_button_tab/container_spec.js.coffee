describe "RadioButtonTab.Container", ->
  beforeEach ->
    loadFixtures("radio_button_tab.html")
    @container = new RadioButtonTab.Container(document.querySelector(RadioButtonTab.SELECTORS.CONTAINER))
    document.getElementById("hoge_piyo-bar-tab").checked = true

  describe "#currentPageTab", ->
    it "は、現在選択されているページのタブ要素を返すこと", ->
      expect(@container.currentPageTab()).toHaveId("hoge_piyo-bar-tab")

  describe "#currentPageContent", ->
    it "は、現在選択されているページの本文要素を返すこと", ->
      expect(@container.currentPageContent()).toHaveId("hoge_piyo-bar-content")
