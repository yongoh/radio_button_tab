require 'rails_helper'

feature "RadioButtonTab", js: true do
  before do
    visit root_path
  end

  context "初期状態" do
    scenario "先頭のページが選択されている" do
      expect(page).to have_checked_field("FOO", visible: false)
      expect(page).not_to have_checked_field("BAR", visible: false)
      expect(page).not_to have_checked_field("BAZ", visible: false)
      expect(page).to have_selector(".page-content", text: "foofoofoofoofoofoofoofoofoofoo")
      expect(page).not_to have_selector(".page-content", text: "barbarbarbarbarbarbarbarbarbar")
      expect(page).not_to have_selector(".page-content", text: "bazbazbazbazbazbazbazbazbazbaz")
    end
  end

  feature "タブ要素をクリックした場合" do
    before do
      find(".radio_button_tab .page-tab:nth-of-type(3)").click
    end

    scenario "クリックしたタブのページを選択" do
      expect(page).not_to have_checked_field("FOO", visible: false)
      expect(page).not_to have_checked_field("BAR", visible: false)
      expect(page).to have_checked_field("BAZ", visible: false)
      expect(page).not_to have_selector(".page-content", text: "foofoofoofoofoofoofoofoofoofoo")
      expect(page).not_to have_selector(".page-content", text: "barbarbarbarbarbarbarbarbarbar")
      expect(page).to have_selector(".page-content", text: "bazbazbazbazbazbazbazbazbazbaz")
    end
  end
end
