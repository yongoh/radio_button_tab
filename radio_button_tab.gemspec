$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "radio_button_tab/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "radio_button_tab"
  spec.version     = RadioButtonTab::VERSION
  spec.authors     = ["yongoh"]
  spec.email       = ["a.yongoh@gmail.com"]
  spec.homepage    = ""
  spec.summary     = ""
  spec.description = ""
  spec.license     = "MIT"

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "core_ext"

  spec.add_development_dependency "rails", "~> 5.2.3"
  spec.add_development_dependency "sqlite3", "1.3.13"
  spec.add_development_dependency "sprockets", "~> 3"
  spec.add_development_dependency "rspec-rails"
  spec.add_development_dependency "rspec-html-matchers"
  spec.add_development_dependency "capybara", "3.15"
  spec.add_development_dependency "poltergeist"
  spec.add_development_dependency "puma"
  spec.add_development_dependency "coffee-rails"
  spec.add_development_dependency "sass"
  spec.add_development_dependency "sass-rails"
end
